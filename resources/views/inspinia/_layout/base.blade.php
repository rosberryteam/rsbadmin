<!DOCTYPE html>
<html lang="en">
<head>
	{!!
		Meta::setTitle(AdminTemplate::makeTitle($title))
			->addMeta(['charset' => 'utf-8'], 'meta::charset')
			->addMeta(['content' => csrf_token(), 'name' => 'csrf-token'])
			->addMeta(['content' => 'width=device-width, initial-scale=1', 'name' => 'viewport'])
			->addMeta(['content' => 'IE=edge', 'http-equiv' => 'X-UA-Compatible'])
			->render()
	!!}

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->

	@stack('scripts')
	<link href="/packages/sleepingowl/default/libs/Inspinia/css/bootstrap.min.css" rel="stylesheet">
    <link href="/packages/sleepingowl/default/libs/Inspinia/css/animate.css" rel="stylesheet">
    <link href="/packages/sleepingowl/default/libs/Inspinia/css/style.css" rel="stylesheet">
</head>
<body class='skin-1'>
	@yield('content')
	<script src="/packages/sleepingowl/default/libs/Inspinia/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

	<!-- Custom and plugin javascript -->
	<script src="/packages/sleepingowl/default/libs/Inspinia/js/inspinia.js"></script>
	<script src="/packages/sleepingowl/default/libs/Inspinia/js/plugins/pace/pace.min.js"></script>

</body>

</html>
