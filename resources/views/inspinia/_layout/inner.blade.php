@extends(AdminTemplate::getViewPath('_layout.base'))

@section('content')
<div id="wrapper">

	@include(AdminTemplate::getViewPath('_partials.navigation'))


    <div id="page-wrapper" class="gray-bg">
		@include(AdminTemplate::getViewPath('_partials.header'))

		<div class="wrapper wrapper-content animated fadeInRight">
           	<div class="row">
                <div class="col-lg-12">
					{!! $content !!}
				</div>
			</div>
		</div>


        <div class="footer">
            <div class="pull-right">
                
            </div>
            <div>
                <strong>Copyright</strong> Rosberry &copy; 2016
            </div>
        </div>

    </div>
</div>

@stop